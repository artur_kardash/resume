-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Мар 14 2016 г., 15:24
-- Версия сервера: 5.5.44-0ubuntu0.14.04.1
-- Версия PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `apercon`
--

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(15) NOT NULL,
  `message` varchar(100) NOT NULL,
  `resume_id` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Дамп данных таблицы `comment`
--

INSERT INTO `comment` (`id`, `firstname`, `message`, `resume_id`) VALUES
(1, 'Иван', 'Достойное резюме!!!', ''),
(2, 'Иван', 'Достойное резюме!!!', ''),
(3, 'Иван', 'Проверочный!!!', ''),
(4, 'Сергей', 'Еще проверка', ''),
(5, 'Иван', 'Проверочный!!!', ''),
(20, 'Gthj', 'bjdncjdn', '37'),
(21, 'dkfjghjk', 'dfknbk', '41'),
(22, 'dkfjghjk', 'dfknbk', '41'),
(23, 'dkfjghjk', 'dfknbk', '41'),
(24, 'dkfjghjk', 'dfknbk', '41'),
(25, 'Петр', 'Проба', '40'),
(26, 'Юрий', 'Сработало!!!', '37'),
(27, 'Сергей', 'Проверочный отзыв!!!', '38');

-- --------------------------------------------------------

--
-- Структура таблицы `resume`
--

CREATE TABLE IF NOT EXISTS `resume` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `date` datetime NOT NULL,
  `file` varchar(100) NOT NULL,
  `status` varchar(15) NOT NULL DEFAULT 'В ожидании',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

--
-- Дамп данных таблицы `resume`
--

INSERT INTO `resume` (`id`, `name`, `date`, `file`, `status`) VALUES
(37, 'Иван', '2016-03-12 20:56:37', 'application_form_2015.doc', ''),
(38, 'Степан', '2016-03-12 21:13:10', 'Sankt-Peterburg.doc', 'Отклонено'),
(39, 'Артем', '2016-03-12 21:18:55', 'Proverka.txt', 'Принято'),
(40, 'Дмитрий', '2016-03-12 21:20:31', 'proverka.txt', ''),
(41, 'Степан', '2016-03-14 13:25:12', 'Sankt-Peterburg.doc', 'В ожидании');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
