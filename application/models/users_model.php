
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users_model extends CI_Model 
{


  public function create ($file)
  {
    return $this->db->insert('resume', $file);
  }

  public function get_list()
  {
    $this->db->select('*');
    $this->db->from('resume');
    $query = $this->db->get();
    if ($query) 
    {
      return $query->result_array();
    }
    return false;
  }

  public function get_status($id)
  {
    $this->db->select('id', 'status');
    $query = $this->db->get_where('resume', array("id"=>$id));
    if ($query) 
    {
       return $query->row_array();
    }
       return false;
  }

  public function update($id, $data)
  {  
    $this->db->where('id', $id);
    $this->db->update('resume', $data);
  }

  public function add_comment($data)
  {
    $query = $this->db->insert('comment', $data);
    if ($query) 
    {
      return true;
    } 
    else 
      {
        return false;
      }
  }

  public function get_comment($id) 
  {
    $this->db->select('*');
    $this->db->from('comment');
    $this->db->join('resume', 'comment.resume_id = resume.id');
    $this->db->where("resume_id",$id);
    $query = $this->db->get();
    if ($query) 
    {
      return $query->result_array();
    }
       return false; 
  }

  public function get_sort_list()
  {
    $this->db->select('*');
    $this->db->from('resume');
    $this->db->order_by("UPPER(name)","ASC");
    $query = $this->db->get();
    if ($query) 
    {
      return $query->result_array();
    }
      return false;
  }
   
  public function get_sort_status()
  {
    $this->db->select('*');
    $this->db->from('resume');
    $this->db->order_by("UPPER(status)","ASC");
    $query = $this->db->get();
    if ($query) 
    {
      return $query->result_array();
    }
      return false;
  }

  public function get_sort_date()
  {
    $this->db->select('*');
    $this->db->from('resume');
    $this->db->order_by("UPPER(date)","ASC");
    $query = $this->db->get();
    if ($query) 
    {
      return $query->result_array();
    }
      return false;
  }

  public function get_sort_resume()
  {
    $this->db->select('*');
    $this->db->from('resume');
    $this->db->order_by("UPPER(file)","ASC");
    $query = $this->db->get();
    if ($query) 
    {
      return $query->result_array();
    }
      return false;
  }

}