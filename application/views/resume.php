<div id='list'>
<h3 style="color:#2b2828"> <span class="glyphicon glyphicon-info-sign" > </span> Все присланные резюме:</h3>
<table class="table table-hover" border="3">
<thead>
	<tr>
		<th>'ID'</th>	
		<th> <a class="btn btn-success" href="/main/sort/">'Имя' </a></th>
		<th> <a class="btn btn-success" href="/main/sort_date/">'Дата подачи заявки'</a> </th>
		<th> <a class="btn btn-success" href="/main/sort_resume/">'Резюме' </a></th>
		<th> <a class="btn btn-success" href="/main/sort_status/">'Статус'<a/> </th>
		<th>'Настройки' </th>
	</tr>
</thead>
<tbody>
<?php foreach ($list as $resume): ?>
	<tr>
		<td><?php echo $resume['id']; ?> </td>
		<td><?php echo $resume['name']; ?></td>
		<td><?php echo $resume['date']; ?></td>
		<td><?php echo $resume['file']; ?></td>
		<td><?php echo $resume['status']; ?></td>
		<td> 
			<a  class="btn btn-success" href="/main/status/<?php echo $resume['id']; ?>">Статус</a> <div class="coll-md-6">
	 		<a  class="btn btn-success" href="/main/comment/<?php echo $resume['id']; ?>">Отзывы</a> <div class="coll-md-6">
	 	</td>

	</tr>
<?php endforeach; ?>
</tbody>
</table>
</div>