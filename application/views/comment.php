<p><h2 style="color: #0d0f0d;padding:20px"> Оставить отзыв</h2></p>
<form class="form-horizontal" action = "/main/comment_db" method = "post">
 <input value = <?php echo $resume_id;?>  type= "hidden" name=  'comment[resume_id]'>
  <div class="form-group">
    <label class="control-label col-xs-3" style="color: #0d0f0d" for="name"> <span class="glyphicon glyphicon-user"></span> Имя *:</label>
    <div class="col-xs-9">
      <input type="text" name= "comment[firstname]" style="width:200px" class="form-control" id="name" placeholder="Введите имя" required="" oninvalid="this.setCustomValidity('Данное поле должно быть заполнено!')" oninput="setCustomValidity('')">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" style="color: #0d0f0d" "width:100px" for="inputPassword"> <span class="glyphicon glyphicon-comment"></span> Отзыв *:</label>
    <div class="col-xs-9">
      <textarea type="message" name= "comment[message]" rows= "7" cols= "70" style="width:350px" class="form-control"  placeholder="Оставьте свой отзыв" required="" oninvalid="this.setCustomValidity('Данное поле должно быть заполнено!')" oninput="setCustomValidity('')"></textarea>
    </div>
  </div>
  <br />
  <div class="form-group">
    <div class="col-xs-offset-3 col-xs-9">
      <input type="submit" class="btn btn-primary" value="Отправить">
      <input type="reset" class="btn btn-default" value="Очистить форму">
    </div>
  </div>

</form>
<div id="comment">
<h3 style="color:#070707"> <span class="glyphicon glyphicon-info-sign" > </span> Все отзывы:</h3>
<table class="table table-hover" border="3">
<thead>
  <tr>
    <th> 'ID'  </th>
    <th> 'Имя сотрудника' </th>
    <th> 'Отзыв' </th>
    <th> 'Имя Отправителя' </th>
  </tr>
</thead>
<tbody>
<?php foreach ($com as $comment): ?>
  <tr>
    <td><?php echo $comment['id']; ?> </td>
    <td><?php echo $comment['firstname']; ?></td>
    <td><?php echo $comment['message']; ?></td>
    <td> <?php echo $comment['name'] ?> </td>
  </tr>
<?php endforeach; ?>
</tbody>
</table>
</div>
