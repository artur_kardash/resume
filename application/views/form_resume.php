<p><h2 style="color: #0d0f0d;padding:20px"> Отправить резюме</h2></p>
<form class="form-horizontal" action = "/main/resume_db" method = "post" enctype="multipart/form-data"> 
  <div class="form-group">
    <label class="control-label col-xs-3" style="color: #0d0f0d" for="name"> <span class="glyphicon glyphicon-user"></span> Имя *:</label>
    <div class="col-xs-9">
      <input type="text" name= "resume[name]" style="width:200px" class="form-control" id="name" placeholder="Введите имя" required="" oninvalid="this.setCustomValidity('Данное поле должно быть заполнено!')" oninput="setCustomValidity('')">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" style="color: #0d0f0d" "width:100px" > <span class="glyphicon glyphicon-comment"></span> Загрузить файл *:</label>
    <div class="col-xs-9">
     <span class="btn btn-success btn-file">
        <input type="file" name="userfile" />
     </span>
    </div>
  </div>
</br>
  <div class="form-group">
    <div class="col-xs-offset-3 col-xs-9">
      <input type="submit" name="download" class="btn btn-primary" value="Отправить">
      <input type="reset" class="btn btn-default" value="Очистить форму">
    </div>
  </div>
</form>

