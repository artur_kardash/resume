<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('users_model');
	}

	public function index()
	{
		$this->home();
	}

	public function home()
	{
		$data['title']='Главная';
		$this->load->view('head', $data);
		$this->load->view('menu');
		$this->load->view('priv');
		$this->load->view('footer');
	}

	public function resume()
	{
		$data['title']='Отправить резюме';
		$this->load->view('head', $data);
		$this->load->view('menu');
		$this->load->view('form_resume');
		$this->load->view('footer');
	}

	public function resume_db()
	{
		$this->load->library('upload');
		$config=array();
		$config['upload_path']='./files/resume/';
		$config['allowed_types']='doc|docx|xls|pdf';
		$this->upload->initialize($config);
		$this->upload->do_upload();
	    $this->load->library("form_validation");
	    $this->form_validation->set_rules('resume[name]', 'Имя', 'trim|required');
	    if($this->form_validation->run())
	    {   	  	
		     $file = $this->input->post('resume');
			 $file['file']=$_FILES['userfile']['name'];
		     $file['date'] = date("Y-m-d H:i:s");
		     $this->load->model('users_model');
		     if ($this->users_model->create($file))
		     {
		       $data['title'] = 'Резюме отправлено';
			   $this->load->view('head', $data);
			   $this->load->view('menu');
			   $this->load->view('success');
			   $this->load->view('footer');
		      }
		      else
		      {
		      	echo "Не отправлено";
		      }
	   }
	}

	public function table_data()
	{
		$data['title'] = 'Таблица резюме';
       	$list_data['list'] = $this->users_model->get_list();
        $this->load->view('head', $data);
        $this->load->view('menu');
        $this->load->view('resume', $list_data);
        $this->load->view('footer');
	}

	public function status($id)
	{
		$data['title']='Изменить статус';
		$status_data['status']=$this->users_model->get_status($id);
		$this->load->view('head', $data);
		$this->load->view('menu');
		$this->load->view('status', $status_data);
		$this->load->view('footer');
	}

	public function status_db()
	{
		$this->load->model('users_model');
		$data=$this->input->post('status');
		$id=$this->input->post('id');
		$this->users_model->update($id, $data);
		$this->table_data();
	}

	public function comment($id)
	{
		$data['title'] = 'Отзывы';
		$comment_data['com']=$this->users_model->get_comment($id);
		$comment_data['resume_id']=$id;
        $this->load->view('head', $data);
        $this->load->view('menu');
        $this->load->view('comment', $comment_data);
        $this->load->view('footer');
	}

	public function comment_db()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('comment[firstname]', 'Имя сотрудника', 'required');
		$this->form_validation->set_rules('comment[message]', 'Отзывы', 'required');
		if($this->form_validation->run())
		{
			$data = $this->input->post('comment');
			$this->users_model->add_comment($data);
			$this->comment($data['resume_id']);
		}
	}

	public function sort()
	{
		$data['title'] = 'Таблица резюме';
       	$list_data['list'] = $this->users_model->get_sort_list();
        $this->load->view('head', $data);
        $this->load->view('menu');
        $this->load->view('resume', $list_data);
        $this->load->view('footer');
	}

	public function sort_status()
	{
		$data['title'] = 'Таблица резюме';
       	$list_data['list'] = $this->users_model->get_sort_status();
        $this->load->view('head', $data);
        $this->load->view('menu');
        $this->load->view('resume', $list_data);
        $this->load->view('footer');
	}

	public function sort_date()
	{
		$data['title'] = 'Таблица резюме';
       	$list_data['list'] = $this->users_model->get_sort_date();
        $this->load->view('head', $data);
        $this->load->view('menu');
        $this->load->view('resume', $list_data);
        $this->load->view('footer');
	}

	public function sort_resume()
	{
		$data['title'] = 'Таблица резюме';
       	$list_data['list'] = $this->users_model->get_sort_resume();
        $this->load->view('head', $data);
        $this->load->view('menu');
        $this->load->view('resume', $list_data);
        $this->load->view('footer');
	}
}

